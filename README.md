ics-ans-eee-rootfs-centos
=========================

Ansible playbook to install an EEE rootfs boot server for CentOS.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
